In order for this to work you must have two files in place:
* ~/.aws/config
* ~/.aws/credentials

~/.aws/config should look like this:
[default]
region = us-west-2
output = json




~/.aws/credentials contains sensitive information, but it will look something like this (replace the XXX with actual values):
[default]
aws_access_key_id = XXXXXXXX
aws_secret_access_key = XXXXXXXX 


(To get the real values for above, please see Brent.)



In order for this to work, make sure your system has a functioning microphone - simply run this and then talk into the microphone and your words should be transcribed.

##############################################################Ideas##############################################################################

***
Source: RG

We may be able to do unicast rtp and have an extension ready, and we originate a call from it and it plays the audio. Command:  

*CLI> channel originate UnicastRTP/127.0.0.1:61001//g722 extension streaming@livestreaming

##############################################################Links##############################################################################

***
https://community.asterisk.org/t/asterisk-15-jack-streams-speech-recognition-so-many-questions/72108/3
https://community.asterisk.org/t/how-to-get-live-audio-stream-of-the-ongoing-call/74251/3

Source: Frank

These were Frank's initial leads.  


***
https://shiffman.net/p5/asterisk/
Source: Frank

Frank thinks this can be implemented with the example script; he may then be able to make a special dial plan to join a channel into that will fead audio through the hybrid script.

***
https://github.com/phsultan/asterisk-eagi-google-speech-recognition
Source: Frank

This may be close to what Frank wants to do, but this is not a Java example. 

***
https://www.joshua-colp.com/2014/broadcasting-asterisk-conferences/

This one may help, but Frank is not sure how we could get the stream back to the ACD / forwarded to Amazon.

***   
https://www.voip-info.org/asterisk-eagi/
Source: Frank

Frank thinks this one looks promising.  


***
https://github.com/asterisk-java/asterisk-java/blob/master/src/integrationtest/org/asteriskjava/fastagi/SpeechDemo.java
Source: RG 


