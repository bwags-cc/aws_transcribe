package com.clearcaptions;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.TargetDataLine;

import javax.sound.sampled.AudioInputStream;
import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.services.transcribestreaming.TranscribeStreamingAsyncClient;
import software.amazon.awssdk.services.transcribestreaming.model.LanguageCode;
import software.amazon.awssdk.services.transcribestreaming.model.MediaEncoding;
import software.amazon.awssdk.services.transcribestreaming.model.StartStreamTranscriptionRequest;
import software.amazon.awssdk.services.transcribestreaming.model.StartStreamTranscriptionResponseHandler;
import software.amazon.awssdk.services.transcribestreaming.model.TranscriptEvent;


public class AWS_Transcribe {

	public static void main(String[] args) throws Exception {
       TranscribeStreamingAsyncClient client = TranscribeStreamingAsyncClient.builder().credentialsProvider(ProfileCredentialsProvider.create()).build();

       StartStreamTranscriptionRequest request = StartStreamTranscriptionRequest.builder()
                                                                                .mediaEncoding(MediaEncoding.PCM)
                                                                                .languageCode(LanguageCode.EN_US)
                                                                                .mediaSampleRateHertz(16_000).build();

       TargetDataLine mic = Microphone.get();
       mic.start();

       AudioStreamPublisher publisher = new AudioStreamPublisher(new AudioInputStream(mic));

       StartStreamTranscriptionResponseHandler response =
           StartStreamTranscriptionResponseHandler.builder().subscriber(e -> {
               //everything in this temp method will run when a 'TranscriptEvent' occurs
               TranscriptEvent event = (TranscriptEvent) e;
               event.transcript().results().forEach(r -> {
                   /*
                   r class: software.amazon.awssdk.services.transcribestreaming.model.Result
                   The reultID can be accessed with r.resultId(), and it changes with EVERY sentence
                   You can know if its a partial with r.isPartial(); == False means Amazon is finished with interpreting the sentence

                   */
                   if (r.isPartial() == false) {
                       r.alternatives().forEach(a -> {
                           /*
                           'a' is of class software.amazon.awssdk.services.transcribestreaming.model.Alternative
                           You can find the number of words in the sentence with a.items().size()

                           */

                           System.out.println(a.transcript());
                       });
                   }
               });
           }).build();

       client.startStreamTranscription(request, publisher, response).join();
   }
}